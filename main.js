const bth = document.querySelector('.btn');
const fas = document.querySelectorAll('.fas');
const passInput = document.querySelectorAll('.password-input');
const cancelButton = document.querySelector(".cancel");

bth.addEventListener('click', () => {
    if (passInput[0].value === passInput[1].value) {
        alert('You are welcome');
    } else {
        document.body.querySelector(".error").style.display = 'initial';

    }
});

fas.forEach(e => {
    const passInput = document.querySelector(`[data-input = '${e.dataset.icon}']`);
    if (e.classList.contains('fa-eye-slash')) {
        passInput.type = "text";
    } else {
        passInput.type = "password";
    }
    e.addEventListener('click', (event) => {

        if (!e.classList.contains('fa-eye-slash')) {
            e.classList.remove('fa-eye');
            e.classList.add('fa-eye-slash');
            passInput.type = "text";
        } else {
            passInput.type = "password";
            e.classList.remove('fa-eye-slash');
            e.classList.add('fa-eye');
        }
    });
});

cancelButton.addEventListener("click", function(event) {
    document.body.querySelector(".error").style.display = 'none';
});